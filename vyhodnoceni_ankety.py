import pandas as pd
import numpy as np

pd.options.mode.copy_on_write = True # To treat copy/write operations correctly

## Inout data
fileName = "FSv_léto_22_23_teachers"
K = 0.5 # Normalization constant

## Import evaluation data (Step 1) 
df = pd.read_csv(fileName + ".csv") # Read the CSV file into a DataFrame

## Filter only the department members (Step 2)
value = ['["11132"]', '["11132","11000"]']
dfTeachers = df[df["departments"].isin(value)]

## Export data to NumPy format
p = dfTeachers['valueAnswer'].to_numpy()
r = dfTeachers['rating'].to_numpy()

## Compute mean variables (Step 3)
P = p.mean()
R = r.mean()

## Computer modified rating (Step 4)
ρ = np.add(r*p, K*P*R, dtype='float64')/np.add(p, K*P, dtype='float64') 

## Sort teacher according to modified rankings (Step 5)
dfTeachers['modifiedRating'] = ρ
dfTeachers.sort_values(by='modifiedRating', inplace=True) 

## Output best evaluations (only three will be used in Step 6)
fw = open(fileName + ".md", "w")

fw.write("## Nejlépe hodnocení pedagogové v [letním/zimním] semestru akademického roku [20aa/20ab]\n")

for row in dfTeachers.itertuples():
    fw.write("\n**" + row.fullName + "**\n")
    fw.write("- původní průměr: {:.2f}\n".format(row.rating))
    fw.write("- modifikovaný průměr: {:.2f}\n".format(row.modifiedRating))
    fw.write("- celkem hlasů: {:d}\n".format(row.valueAnswer))