## Nejlépe hodnocení pedagogové v [letním/zimním] semestru akademického roku [20aa/20ab]

**Anonymní vyučující 06**
- původní průměr: 1.00
- modifikovaný průměr: 1.12
- celkem hlasů: 18

**Anonymní vyučující 10**
- původní průměr: 1.00
- modifikovaný průměr: 1.13
- celkem hlasů: 15

**Anonymní vyučující 04**
- původní průměr: 1.05
- modifikovaný průměr: 1.14
- celkem hlasů: 22

**Anonymní vyučující 20**
- původní průměr: 1.00
- modifikovaný průměr: 1.18
- celkem hlasů: 10

**Anonymní vyučující 09**
- původní průměr: 1.13
- modifikovaný průměr: 1.23
- celkem hlasů: 16

**Anonymní vyučující 03**
- původní průměr: 1.20
- modifikovaný průměr: 1.26
- celkem hlasů: 25

**Anonymní vyučující 37**
- původní průměr: 1.00
- modifikovaný průměr: 1.30
- celkem hlasů: 4

**Anonymní vyučující 35**
- původní průměr: 1.00
- modifikovaný průměr: 1.30
- celkem hlasů: 4

**Anonymní vyučující 34**
- původní průměr: 1.00
- modifikovaný průměr: 1.30
- celkem hlasů: 4

**Anonymní vyučující 02**
- původní průměr: 1.26
- modifikovaný průměr: 1.30
- celkem hlasů: 27

**Anonymní vyučující 01**
- původní průměr: 1.29
- modifikovaný průměr: 1.33
- celkem hlasů: 28

**Anonymní vyučující 23**
- původní průměr: 1.22
- modifikovaný průměr: 1.33
- celkem hlasů: 9

**Anonymní vyučující 38**
- původní průměr: 1.00
- modifikovaný průměr: 1.33
- celkem hlasů: 3

**Anonymní vyučující 14**
- původní průměr: 1.31
- modifikovaný průměr: 1.37
- celkem hlasů: 13

**Anonymní vyučující 05**
- původní průměr: 1.33
- modifikovaný průměr: 1.37
- celkem hlasů: 18

**Anonymní vyučující 33**
- původní průměr: 1.25
- modifikovaný průměr: 1.40
- celkem hlasů: 4

**Anonymní vyučující 12**
- původní průměr: 1.40
- modifikovaný průměr: 1.43
- celkem hlasů: 15

**Anonymní vyučující 44**
- původní průměr: 1.00
- modifikovaný průměr: 1.44
- celkem hlasů: 1

**Anonymní vyučující 43**
- původní průměr: 1.00
- modifikovaný průměr: 1.44
- celkem hlasů: 1

**Anonymní vyučující 46**
- původní průměr: 1.00
- modifikovaný průměr: 1.44
- celkem hlasů: 1

**Anonymní vyučující 45**
- původní průměr: 1.00
- modifikovaný průměr: 1.44
- celkem hlasů: 1

**Anonymní vyučující 39**
- původní průměr: 1.33
- modifikovaný průměr: 1.45
- celkem hlasů: 3

**Anonymní vyučující 26**
- původní průměr: 1.50
- modifikovaný průměr: 1.51
- celkem hlasů: 8

**Anonymní vyučující 40**
- původní průměr: 1.50
- modifikovaný průměr: 1.52
- celkem hlasů: 2

**Anonymní vyučující 18**
- původní průměr: 1.55
- modifikovaný průměr: 1.54
- celkem hlasů: 11

**Anonymní vyučující 07**
- původní průměr: 1.59
- modifikovaný průměr: 1.57
- celkem hlasů: 17

**Anonymní vyučující 31**
- původní průměr: 1.83
- modifikovaný průměr: 1.69
- celkem hlasů: 6

**Anonymní vyučující 08**
- původní průměr: 1.76
- modifikovaný průměr: 1.70
- celkem hlasů: 17

**Anonymní vyučující 17**
- původní průměr: 1.82
- modifikovaný průměr: 1.72
- celkem hlasů: 11

**Anonymní vyučující 25**
- původní průměr: 2.00
- modifikovaný průměr: 1.81
- celkem hlasů: 8

**Anonymní vyučující 22**
- původní průměr: 2.00
- modifikovaný průměr: 1.82
- celkem hlasů: 9

**Anonymní vyučující 16**
- původní průměr: 2.00
- modifikovaný průměr: 1.85
- celkem hlasů: 12

**Anonymní vyučující 27**
- původní průměr: 2.13
- modifikovaný průměr: 1.89
- celkem hlasů: 8

**Anonymní vyučující 24**
- původní průměr: 2.13
- modifikovaný průměr: 1.89
- celkem hlasů: 8

**Anonymní vyučující 19**
- původní průměr: 2.18
- modifikovaný průměr: 1.97
- celkem hlasů: 11

**Anonymní vyučující 15**
- původní průměr: 2.17
- modifikovaný průměr: 1.97
- celkem hlasů: 12

**Anonymní vyučující 11**
- původní průměr: 2.33
- modifikovaný průměr: 2.12
- celkem hlasů: 15

**Anonymní vyučující 29**
- původní průměr: 2.86
- modifikovaný průměr: 2.29
- celkem hlasů: 7

**Anonymní vyučující 32**
- původní průměr: 3.20
- modifikovaný průměr: 2.34
- celkem hlasů: 5
